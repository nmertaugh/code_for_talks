require 'spec_helper'

RSpec.describe SmsSender do
  subject(:sender) { SmsSender.new(user) }

  describe '#push' do
    context 'when user is on block list' do
      let(:user) { User.new(phone_type: :foo).save }

      before { BlockList.add(user) }

      it 'logs bad sms target' do
        allow($stdout).to receive(:puts)

        sender.push 'foo message'

        expect($stdout).to have_received(:puts)
          .with("Attempted to send SMS to bad user #{user.id}")
          .exactly(:once)
      end
    end

    context 'when user is NOT on block list' do
      before { BlockList.clear }

      context 'when user accepts SMS' do
        let(:user) { User.new(phone_type: :mobile).save }

        context 'and user is unsubscribed from this thread' do
          before { user.unsubscribed_threads << :this_thread }

          context 'and global unsubscribe is on' do
            around(:example) do |example|
              ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => 'true' do
                example.run
              end
            end

            it 'does NOT delegate to SmsGateway#send' do
              allow(SmsInterface::SmsGateway).to receive(:push)

              sender.push 'foo message', thread: :this_thread

              expect(SmsInterface::SmsGateway).not_to have_received(:push)
            end
          end

          context 'and global unsubscribe is NOT on' do
            around(:example) do |example|
              ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => nil do
                example.run
              end
            end

            it 'does NOT delegate to SmsGateway#send' do
              allow(SmsInterface::SmsGateway).to receive(:push)

              sender.push 'foo message', thread: :this_thread

              expect(SmsInterface::SmsGateway).not_to have_received(:push)
            end
          end
        end

        context 'and user is NOT unsubscribed from this thread' do
          before { user.unsubscribed_threads << :another_thread }

          context 'and global unsubscribe is on' do
            around(:example) do |example|
              ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => 'true' do
                example.run
              end
            end

            it 'does NOT delegate to SmsGateway#send' do
              allow(SmsInterface::SmsGateway).to receive(:push)

              sender.push 'foo message', thread: :this_thread

              expect(SmsInterface::SmsGateway).not_to have_received(:push)
            end
          end

          context 'and global unsubscribe is NOT on' do
            around(:example) do |example|
              ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => nil do
                example.run
              end
            end

            it 'delegates to SmsGateway#send' do
              allow(SmsInterface::SmsGateway).to receive(:push)

              sender.push 'foo message', thread: :this_thread

              expect(SmsInterface::SmsGateway).to have_received(:push)
                .with(user, 'foo message')
                .exactly(:once)
            end
          end
        end
      end

      context 'when user does NOT accept SMS' do
        let(:user) { User.new(phone_type: :home).save }

        context 'and user is unsubscribed from this thread' do
          before { user.unsubscribed_threads << :this_thread }

          context 'and global unsubscribe is on' do
            around(:example) do |example|
              ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => 'true' do
                example.run
              end
            end

            it 'does NOT delegate to SmsGateway#send' do
              allow(SmsInterface::SmsGateway).to receive(:push)

              sender.push 'foo message', thread: :this_thread

              expect(SmsInterface::SmsGateway).not_to have_received(:push)
            end
          end

          context 'and global unsubscribe is NOT on' do
            around(:example) do |example|
              ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => nil do
                example.run
              end
            end

            it 'does NOT delegate to SmsGateway#send' do
              allow(SmsInterface::SmsGateway).to receive(:push)

              sender.push 'foo message', thread: :this_thread

              expect(SmsInterface::SmsGateway).not_to have_received(:push)
            end
          end
        end

        context 'and user is NOT unsubscribed from this thread' do
          before { user.unsubscribed_threads << :another_thread }

          context 'and global unsubscribe is on' do
            around(:example) do |example|
              ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => 'true' do
                example.run
              end
            end

            it 'does NOT delegate to SmsGateway#send' do
              allow(SmsInterface::SmsGateway).to receive(:push)

              sender.push 'foo message', thread: :this_thread

              expect(SmsInterface::SmsGateway).not_to have_received(:push)
            end
          end

          context 'and global unsubscribe is NOT on' do
            around(:example) do |example|
              ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => nil do
                example.run
              end
            end

            it 'does NOT delegate to SmsGateway#send' do
              allow(SmsInterface::SmsGateway).to receive(:push)

              sender.push 'foo message', thread: :this_thread

              expect(SmsInterface::SmsGateway).not_to have_received(:push)
            end
          end
        end
      end
    end
  end
end
