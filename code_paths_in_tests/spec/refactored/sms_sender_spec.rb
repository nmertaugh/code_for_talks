ENV['REFACTOR'] = 'true'
require 'spec_helper'

RSpec.describe SmsSender do
  subject(:sender) { SmsSender.new(user) }
  let(:interface) { class_double(SmsInterface, push: true) }
  let(:message_body) { 'foo' }
  let(:phone_number) { '1234567890' }
  let(:user) { instance_double(User, phone_number: phone_number) }

  describe '#push' do
    context 'when thread is NOT specified' do
      before { setup_for_thread_and_exercise(:general) }

      it 'delegates to interface with :general thread' do
        expect_interface_to_have_received_push_with_correct_args
      end
    end

    context 'when thread is specified' do
      before { setup_for_thread_and_exercise(:bar) }

      it 'delegates to interface with specified thread' do
        expect_interface_to_have_received_push_with_correct_args
      end
    end

    def expect_interface_to_have_received_push_with_correct_args
      expect(interface).to have_received(:push)
        .with(user, message_body)
        .exactly(:once)
    end

    def setup_for_thread_and_exercise(thread)
      allow(SmsInterface).to receive(:for)
        .with(user, thread)
        .and_return(interface)

      if thread == :general
        sender.push message_body
      else
        sender.push message_body, thread: thread
      end
    end
  end
end
