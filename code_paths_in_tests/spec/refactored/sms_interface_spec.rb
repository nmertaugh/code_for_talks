ENV['REFACTOR'] = 'true'
require 'spec_helper'

RSpec.describe SmsInterface do
  subject(:interface) { SmsInterface }
  let(:user) { instance_double(User) }

  describe '.for' do
    context 'when BlockList includes user' do
      before { allow(BlockList).to receive(:include?).with(user).and_return(true) }

      it 'returns SmsInterface::BadTargetLogger' do
        expect(interface.for(user, :foo_thread)).to eq(SmsInterface::BadTargetLogger)
      end
    end

    context 'when GLOBAL_UNSUBSCRIBE_FLAG set' do
      around(:example) do |example|
        ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => 'true' do
          example.run
        end
      end

      it 'returns SmsInterface::NullSender' do
        expect(interface.for(user, :foo_thread)).to eq(SmsInterface::NullSender)
      end
    end

    context 'when user is ineligible for SMS' do
      before { allow(user).to receive(:ineligible_for_sms?).with(:foo_thread).and_return(true) }

      it 'returns SmsInterface::NullSender' do
        expect(interface.for(user, :foo_thread)).to eq(SmsInterface::NullSender)
      end
    end

    context 'when user not blocked, no global flag set, and user is eligible for SMS' do
      around(:example) do |example|
        ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => nil do
          example.run
        end
      end

      before do
        allow(user).to receive(:ineligible_for_sms?).with(:foo_thread).and_return(false)
        allow(BlockList).to receive(:include?).with(user).and_return(false)
      end

      it 'returns SmsInterface::SmsGateway' do
        expect(interface.for(user, :foo_thread)).to eq(SmsInterface::SmsGateway)
      end
    end
  end
end
