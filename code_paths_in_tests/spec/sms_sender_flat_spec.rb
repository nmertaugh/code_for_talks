require 'spec_helper'

RSpec.describe SmsSender do
  let(:query_chain_double) { double }
  subject(:sender) { SmsSender.new(user) }
  let(:user) { User.new }

  describe '#push' do
    before do
      # CODE-SMELL: long stub chains to avoid persistence
      allow(User).to receive(:find).with(user.id).and_return(user)
    end

    shared_context 'when user is on block list' do
      before do
        # CODE-SMELL: long stub chains to avoid persistence
        allow(BlockList).to receive(:include?).with(user).and_return(true)
        allow(BlockList).to receive(:users).and_return(query_chain_double)
        allow(query_chain_double).to receive(:where).with(id: user.id).and_return([user])
      end
    end

    shared_context 'when user is NOT on block list' do
      before do
        # CODE-SMELL: long stub chains to avoid persistence
        allow(BlockList).to receive(:include?).with(user).and_return(false)
        allow(BlockList).to receive(:users).and_return(query_chain_double)
        allow(query_chain_double).to receive(:where).with(id: user.id).and_return([])
      end
    end

    shared_context 'when user accepts SMS' do
      before { user.phone_type = :mobile }
    end

    shared_context 'when user does NOT accept SMS' do
      before { user.phone_type = :home }
    end

    shared_context 'when user is unsubscribed from this thread' do
      before { user.unsubscribed_threads << :this_thread }
    end

    shared_context 'when user is NOT unsubscribed from this thread' do
      before { user.unsubscribed_threads.clear }
    end

    shared_context 'when global unsubscribe is on' do
      around(:example) do |example|
        ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => 'true' do
          example.run
        end
      end
    end

    shared_context 'when global unsubscribe is NOT on' do
      around(:example) do |example|
        ClimateControl.modify 'GLOBAL_UNSUBSCRIBE_FLAG' => nil do
          example.run
        end
      end
    end

    shared_examples 'it logs bad sms target' do
      it 'logs bad sms target' do
        allow($stdout).to receive(:puts)

        sender.push 'foo message'

        expect($stdout).to have_received(:puts)
          .with("Attempted to send SMS to bad user #{user.id}")
          .exactly(:once)
      end
    end

    shared_examples 'it does NOT delegate to SmsGateway#send' do
      it 'does NOT delegate to SmsGateway#send' do
        allow(SmsInterface::SmsGateway).to receive(:push)

        sender.push 'foo message', thread: :this_thread

        expect(SmsInterface::SmsGateway).not_to have_received(:push)
      end
    end

    shared_examples 'it delegates to SmsGateway#send' do
      it 'does delegates to SmsGateway#send' do
        allow(SmsInterface::SmsGateway).to receive(:push)

        sender.push 'foo message', thread: :this_thread

        expect(SmsInterface::SmsGateway).to have_received(:push)
          .with(user, 'foo message')
          .exactly(:once)
      end
    end

    context 'when user is on block list' do
      include_context 'when user is on block list'

      include_examples 'it logs bad sms target'
    end

    context 'when NOT blocked, accepting SMS, NOT unsubscribed, and global unsubscribe is NOT on' do
      include_context 'when user is NOT on block list'
      include_context 'when user accepts SMS'
      include_context 'when user is NOT unsubscribed from this thread'
      include_context 'when global unsubscribe is NOT on'

      include_examples 'it delegates to SmsGateway#send'
    end

    context 'when NOT blocked, accepting SMS, NOT unsubscribed, and global unsubscribe is on' do
      include_context 'when user is NOT on block list'
      include_context 'when user accepts SMS'
      include_context 'when user is NOT unsubscribed from this thread'
      include_context 'when global unsubscribe is on'

      include_examples 'it does NOT delegate to SmsGateway#send'
    end

    context 'when NOT blocked, NOT accepting SMS, NOT unsubscribed, global unsubscribe is NOT on' do
      include_context 'when user is NOT on block list'
      include_context 'when user does NOT accept SMS'
      include_context 'when user is NOT unsubscribed from this thread'
      include_context 'when global unsubscribe is NOT on'

      include_examples 'it does NOT delegate to SmsGateway#send'
    end

    context 'when NOT blocked, accepting SMS, NOT unsubscribed, and global unsubscribe is NOT on' do
      include_context 'when user is NOT on block list'
      include_context 'when user accepts SMS'
      include_context 'when user is unsubscribed from this thread'
      include_context 'when global unsubscribe is NOT on'

      include_examples 'it does NOT delegate to SmsGateway#send'
    end
  end
end
