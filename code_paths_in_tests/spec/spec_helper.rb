require 'climate_control'
require 'pry'
require 'rspec'

Dir["./lib/shared/*.rb"].each { |file| require file }

if ENV['REFACTOR']
  Dir["./lib/well_factored/**/*.rb"].each { |file| require file }
else
  Dir["./lib/ill_factored/**/*.rb"].each { |file| require file }
end
