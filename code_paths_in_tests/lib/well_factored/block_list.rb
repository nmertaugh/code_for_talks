class BlockList
  @@users = []

  def self.add(user)
    DatabaseMonitor.warn

    (@@users << user).uniq!
  end

  def self.clear
    @@users = []
  end

  def self.include?(user)
    @@users.include? user
  end
end
