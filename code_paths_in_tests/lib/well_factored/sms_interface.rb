class SmsInterface
  def self.for(user, thread)
    new(user, thread).for
  end

  def self.push(_user, _message)
    raise 'Subclass must implement'
  end

  def initialize(user, thread)
    @user = user
    @thread = thread
  end

  def for
    if blocked?
      BadTargetLogger
    elsif ineligible?
      NullSender
    else
      SmsGateway
    end
  end

  private

  def blocked?
    BlockList.include?(@user)
  end

  def ineligible?
    ENV['GLOBAL_UNSUBSCRIBE_FLAG'] || @user.ineligible_for_sms?(@thread)
  end
end

#  12.6: flog total
#  1.8: flog/method average
#
#  3.0: SmsInterface#for                 lib/well_factored/sms_interface.rb:15-21
#  2.4: SmsInterface#ineligible?         lib/well_factored/sms_interface.rb:31-32
#  2.2: SmsInterface::for                lib/well_factored/sms_interface.rb:2-3

# 4 Code Paths:
#
#                           blocked?
#                       /y            n\
#                     /                 \
#             BadTargetLogger          ENV[]?
#                                   /y        n\
#                                 /             \
#                           NullSender        @user.ineligible?
#                                           /y                  n\
#                                         /                        \
#                                     NullSender                SmsGateway
