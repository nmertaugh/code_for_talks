class SmsInterface
  class BadTargetLogger < SmsInterface
    def self.push(user, _)
      puts "Attempted to send SMS to bad user #{user.id}"
    end
  end
end
