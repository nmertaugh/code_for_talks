class SmsSender
  def initialize(user)
    @user = user
  end

  def push(message, thread: :general)
    interface = SmsInterface.for(@user, thread)
    interface.push @user, message
  end
end

#  4.4: flog total
#  2.2: flog/method average
#
#  3.4: SmsSender#push       lib/refactored/sms_sender.rb:7-9
#  1.0: SmsSender#initialize lib/refactored/sms_sender.rb:3-4
