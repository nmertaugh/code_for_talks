class User < PersistedUser
  def ineligible_for_sms?(thread)
    unable_to_receive_sms? || unsubscribed_from_thread?(thread)
  end

  private

  def unable_to_receive_sms?
    @phone_type != :mobile
  end

  def unsubscribed_from_thread?(thread)
    unsubscribed_threads.include? thread
  end
end
