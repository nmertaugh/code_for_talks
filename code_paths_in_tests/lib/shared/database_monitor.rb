class DatabaseMonitor
  def self.warn
    return if ENV['SILENCE_DB_WARNINGS']

    puts "\nYOU HIT THE DATABASE!\n"
  end
end
