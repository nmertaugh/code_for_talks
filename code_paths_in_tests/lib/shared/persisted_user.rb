class PersistedUser
  attr_accessor :phone_type, :unsubscribed_threads
  attr_reader :id, :phone_number

  @@users = []

  def self.find(id)
    DatabaseMonitor.warn

    @@users.find { |user| user.id == id }
  end

  def initialize(phone_type: nil, phone_number: nil)
    @id = rand(100)
    @phone_type = phone_type
    @unsubscribed_threads = []
    @phone_number ||= '1234567890'
  end

  def save
    DatabaseMonitor.warn

    (@@users << self).uniq!
    self
  end

  def to_i
    id
  end
end
