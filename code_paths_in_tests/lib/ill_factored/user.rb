class User < PersistedUser
  def accepts_sms?
    @phone_type == :mobile
  end
end
