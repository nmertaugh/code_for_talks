class SmsSender
  def initialize(user_id)
    @user = User.find(user_id.to_i)
  end

  def push(message, thread: :general)
    if on_block_list?(@user)
      log_bad_sms_target
    elsif !@user.accepts_sms? || (@user.accepts_sms? && @user.unsubscribed_threads.include?(thread)) || ENV['GLOBAL_UNSUBSCRIBE_FLAG']
      return
    else
      SmsInterface::SmsGateway.push @user, message
    end
  end

  private

  def log_bad_sms_target
    puts "Attempted to send SMS to bad user #{@user.id}"
  end

  def on_block_list?(user)
    BlockList.users.where(id: user.id).any?
  end
end


#   23.8: flog total
#   4.8: flog/method average
#
#   14.2: SmsSender#push                  lib/sms_sender.rb:6-12
#   5.0: SmsSender#on_block_list?         lib/sms_sender.rb:22-23
#   2.2: SmsSender#log_bad_sms_target     lib/sms_sender.rb:18-19
#   1.4: SmsSender#initialize             lib/sms_sender.rb:2-3
#   1.0: SmsSender#none

# 8 Code Paths:
#
#                           blocked?
#                       /y            n\
#                     /                 \
#           log_bad_sms_target     @user.accepts_sms?
#                                   /y        n\
#                                 /             \
#                           thread.nil?          nil
#                          /           \
#                         /y           n\
#                        /               \
#                       /                 \
# unsubscribed from default thread?        unsubscribed from passed thread?
#          /y         n\                            /y         n\
#        /               \                         /             \
#      nil             ENV['']?                 nil             ENV['']?
#                    /y        n\                              /y      n\
#                  /             \                            /          \
#                nil            push                        nil          push
#
# Flog scores in perspective: a flog score of about 30 will probably have something like 20-ish
# code paths, which would probably take about 300-ish lines of code to test (unflattened).
#
# We have a 269 line `if` statement in ApplicationHelper::number_to_phone that scores 1847 in flog.
# It's fascinating. Given the loose heuristic above, we might guess that fully testing this method
# would take approximately 18,000 lines of code...
