class BlockList
  @@users = []

  def self.add(user)
    DatabaseMonitor.warn

    (@@users << user).uniq!
  end

  def self.clear
    @@users = []
  end

  def self.users
    QueryChain.new(@@users)
  end

  class QueryChain
    def initialize(user_list)
      @user_list = user_list
    end

    def where(params)
      DatabaseMonitor.warn

      matching_users =
        @user_list.find do |user|
          params.map do |key, value|
            user.send(key) == value
          end.all?
        end

      Array(matching_users)
    end
  end
end
